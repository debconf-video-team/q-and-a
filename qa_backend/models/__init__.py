from qa_backend.models.questions import Question, Questions
from qa_backend.models.talks import Talk, TalkStatus, Talks
from qa_backend.models.users import Permission, User, Users


async def initialize_managers(app):
    database = app['database']
    app['questions'] = Questions(database)
    app['talks'] = Talks(database)
    app['users'] = Users(database)
