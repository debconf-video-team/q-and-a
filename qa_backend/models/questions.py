import dataclasses
from datetime import datetime
from typing import Dict, Optional, Set

from sqlalchemy.sql import select, func
from qa_backend import database as db
from qa_backend.models.manager import Manager


@dataclasses.dataclass
class Question:
    question_id: int
    talk_id: int
    body: str
    author: int
    votes: int
    created_at: datetime
    updated_at: datetime
    deleted_at: Optional[datetime]
    answered_at: Optional[datetime]

    def as_ws_dict(self):
        return dataclasses.asdict(self)


class Questions(Manager):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # TODO: manage cache contents
        self.cache_by_talk_id = {}

    async def get_by_id(self, question_id: int) -> Question:
        if question_id not in self.cache_by_id:
            async with self.db.acquire() as conn:
                async with conn.begin(isolation_level='REPEATABLE READ'):
                    rp = await conn.execute(
                        db.questions.select(
                            db.questions.c.question_id == question_id))
                    question_data = await rp.fetchone()
                    rp = await conn.execute(
                        select([
                            func.count(db.votes.c.vote_id).label('votes'),
                        ])
                        .select_from(db.votes)
                        .where(db.votes.c.question_id == question_id))
                    votes = await rp.scalar()
            question = Question(votes, **dict(question_data.items()))
            self.cache_by_id[question_id] = question
        return self.cache_by_id[question_id]

    async def get_talk_questions(self, talk_id: int) -> Dict[int, Question]:
        return await self._get_talk_questions(talk_id)

    async def _get_talk_questions(self, talk_id: int) -> Dict[int, Question]:
        async with self.db.acquire() as conn:
            async with conn.begin(isolation_level='REPEATABLE READ'):
                questions = {}
                async for row in conn.execute(
                        db.questions.select(db.questions.c.talk_id == talk_id)):
                    question = Question(votes=0, **dict(row.items()))
                    self.cache_by_id[question.question_id] = question
                    questions[question.question_id] = question
                async for row in conn.execute(
                        select([
                            db.questions.c.question_id,
                            func.count(db.votes.c.vote_id).label('votes'),
                        ])
                        .select_from(db.questions.join(db.votes))
                        .where(db.questions.c.talk_id == talk_id)
                        .group_by(db.questions.c.question_id)):
                    row = dict(row.items())
                    questions[row['question_id']].votes = row['votes']
                self.cache_by_talk_id[talk_id] = questions
        return questions

    async def ask_question(self, talk_id: int, body: str, author: int
            ) -> Question:
        async with self.db.acquire() as conn:
            async with conn.begin():
                data = {
                    'body': body,
                    'talk_id': talk_id,
                    'author': author,
                }
                return await self._create(data, conn)

    async def _create(self, data: dict, conn) -> Question:
        assert 'question_id' not in data
        data.setdefault('answered_at', None)
        data.setdefault('deleted_at', None)
        data.setdefault('created_at', datetime.now().astimezone())
        data.setdefault('updated_at', datetime.now().astimezone())
        # Create the question early, to validate the structure
        question = Question(
            question_id=-1,
            votes=0,
            **data)
        rp = await conn.execute(
            db.questions.insert()
            .values(**data)
        )
        r = await rp.fetchone()
        question.question_id = r.question_id
        self.cache_by_id[question.question_id] = question
        if question.talk_id in self.cache_by_talk_id:
            self.cache_by_talk_id[question.talk_id] = question
        return question

    async def vote(self, question_id: int, user_id: int) -> Question:
        question = await self.get_by_id(question_id)
        assert question.author != user_id
        async with self.db.acquire() as conn:
            async with conn.begin('REPEATABLE READ'):
                rp = await conn.execute(
                    db.votes.select(
                        (db.votes.c.question_id == question_id) &
                        (db.votes.c.user_id == user_id)
                    ))
                r = await rp.fetchone()
                if r:
                    return question

                rp = await conn.execute(
                    db.votes.insert().values(
                        question_id=question_id,
                        user_id=user_id,
                    ))
                await rp.fetchone()
        question.votes += 1
        return question

    async def unvote(self, question_id: int, user_id: int) -> Question:
        question = await self.get_by_id(question_id)
        async with self.db.acquire() as conn:
            rp = await conn.execute(
                db.votes.delete(
                    (db.votes.c.question_id == question_id) &
                    (db.votes.c.user_id == user_id)
                ))
            r = rp.rowcount
        question.votes -= r
        return question

    async def voted_by_user(self, user_id: int) -> Set[int]:
        votes = set()
        async with self.db.acquire() as conn:
            async for row in conn.execute(
                    db.votes.select(db.votes.c.user_id == user_id)):
                votes.add(row['question_id'])
        return votes
