import dataclasses
from datetime import datetime, timedelta
from enum import Enum
from typing import List, Optional

from qa_backend import database as db
from qa_backend.models.manager import Manager


class TalkStatus(Enum):
    ACTIVE = 'active'
    CLOSED = 'closed'


@dataclasses.dataclass
class Talk:
    talk_id: int
    title: str
    speakers: str
    status: TalkStatus
    start_at: datetime
    end_at: datetime
    created_at: datetime
    updated_at: datetime
    deleted_at: Optional[datetime]

    def as_ws_dict(self):
        return dataclasses.asdict(self)


class Talks(Manager):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cache_by_id = {}  # We want to keep all talks in memory
        self.last_updated = datetime(1970, 1, 1).astimezone()

    async def get_all(self) -> List[Talk]:
        ttl = timedelta(minutes=5)
        if self.last_updated < datetime.now().astimezone() - ttl:
            await self.refresh_all()
        return self.cache_by_id

    async def refresh_all(self):
        updated_at = datetime.now().astimezone()
        async with self.db.acquire() as conn:
            async for row in conn.execute(db.talks.select(
                    (db.talks.c.updated_at > self.last_updated) |
                    (db.talks.c.deleted_at > self.last_updated))):
                talk = Talk(**dict(row.items()))
                self.cache_by_id[talk.talk_id] = talk
        self.last_updated = updated_at
