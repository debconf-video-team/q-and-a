from weakref import WeakValueDictionary


class Manager:
    def __init__(self, database):
        self.cache_by_id = WeakValueDictionary()
        self.db = database
