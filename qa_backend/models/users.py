import dataclasses
from datetime import datetime
from enum import Enum
from typing import Dict, List, Optional

from passlib.hash import bcrypt
from sqlalchemy.sql import func

from qa_backend import database as db
from qa_backend.models.exceptions import NotFound
from qa_backend.models.manager import Manager


class Permission(Enum):
    ADMIN = 'admin'
    MODERATOR = 'moderator'
    USER = 'user'


@dataclasses.dataclass
class User:
    user_id: int
    fullname: str
    password_hash: Optional[str]
    avatar_url: str
    sso_id: Optional[int]
    email: str
    permissions: Permission
    activated: bool
    created_at: datetime
    updated_at: datetime
    deleted_at: Optional[datetime]

    def as_ws_dict(self):
        d = dataclasses.asdict(self)
        d.pop('email')
        d.pop('password_hash')
        d.pop('created_at')
        d.pop('updated_at')
        d.pop('deleted_at')
        return d


class Users(Manager):
    async def sso_auth(self, data: dict) -> User:
        async with self.db.acquire() as conn:
            async with conn.begin():
                try:
                    user = await self._get_by_sso_id(data['sso_id'], conn)
                    user = await self._update(user, data, conn)
                except NotFound:
                    user = await self._create(data, conn)
                return user

    async def password_auth(self, email: str, password: str) -> Optional[User]:
        user: Optional[User]
        async with self.db.acquire() as conn:
            async with conn.begin():
                try:
                    user = await self._get_by_email(email, conn)
                    password_hash = user.password_hash
                except NotFound:
                    user = None
                    password_hash = None
                # For constant-time failures
                if not password_hash:
                    password_hash = '$2b$12$' + '.' * 53
        if not bcrypt.verify(password, password_hash) or not user:
            return None
        return user

    async def get_by_id(self, user_id: int) -> User:
        if user_id not in self.cache_by_id:
            async with self.db.acquire() as conn:
                await self._get_by_id(user_id, conn)
        return self.cache_by_id[user_id]

    async def get_by_ids(self, user_ids: List[int]) -> Dict[int, User]:
        users = {}
        for user_id in user_ids:
            if user_id in self.cache_by_id:
                users[user_id] = self.cache_by_id[user_id]

        # TODO: Optimize into a single query
        missing = set(user_ids) - set(users.keys())
        if missing:
            async with self.db.acquire() as conn:
                for user_id in user_ids:
                    users[user_id] = await self._get_by_id(user_id, conn)

        return users

    async def _get_by_id(self, user_id: int, conn) -> User:
        rp = await conn.execute(
            db.users.select(db.users.c.user_id == user_id)
        )
        r = await rp.fetchone()
        return self._build_and_cache_user(r)

    async def _get_by_email(self, email: str, conn) -> User:
        if email is None:
            raise NotFound()
        rp = await conn.execute(
            db.users.select(
                (db.users.c.email == email) &
                (db.users.c.deleted_at == None))
        )
        r = await rp.fetchone()
        return self._build_and_cache_user(r)

    async def _get_by_sso_id(self, sso_id: int, conn) -> User:
        rp = await conn.execute(
            db.users.select(
                (db.users.c.sso_id == sso_id) &
                (db.users.c.deleted_at == None))
        )
        r = await rp.fetchone()
        return self._build_and_cache_user(r)

    def _build_and_cache_user(self, result) -> User:
        if not result:
            raise NotFound()
        user = User(**dict(result.items()))
        self.cache_by_id[user.user_id] = user
        return user

    async def _create(self, data: dict, conn) -> User:
        assert 'user_id' not in data
        data.setdefault('activated', True)
        data.setdefault('deleted_at', None)
        data.setdefault('password_hash', None)
        data.setdefault('permissions', 'user')
        data.setdefault('created_at', datetime.now().astimezone())
        data.setdefault('updated_at', datetime.now().astimezone())
        # Create the user early, to validate the structure
        user = User(
            user_id=-1,
            **data)
        rp = await conn.execute(
            db.users.insert()
            .values(**data)
        )
        r = await rp.fetchone()
        user.user_id = r.user_id
        self.cache_by_id[user.user_id] = user
        return user

    async def _update(self, user: User, data: dict, conn) -> User:
        assert 'user_id' not in data
        user = dataclasses.replace(user, **data)
        await conn.execute(
            db.users.update()
            .where(db.users.c.user_id == user.user_id)
            .values(
                updated_at=func.current_timestamp(),
                **data)
        )
        self.cache_by_id[user.user_id] = user
        return user
