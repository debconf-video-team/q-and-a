import datetime
import json


def _json_default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()


def json_dumps(obj):
    """A json dumps() that encodes datetime"""
    return json.dumps(obj, default=_json_default)


def frontend_error(message, key='backend', priority=50):
    """Build an error_add message, that displays on the frontend"""
    return {
        'type': 'error_add',
        'data': {
            'key': key,
            'message': message,
            'priority': priority,
        },
    }
