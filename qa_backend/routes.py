import logging
from asyncio import CancelledError

from aiohttp import WSMsgType, hdrs, web

from qa_backend.auth import get_auth_config_message
from qa_backend.handlers.talks import list_talks
from qa_backend.handlers.questions import questions_voted_by_user
from qa_backend.utils import json_dumps
from qa_backend.ws import MsgRequest, msg_routes


log = logging.getLogger(__name__)
routes = web.RouteTableDef()


@routes.get('/')
async def root(request):
    return web.FileResponse('frontend/dist/index.html', headers={
        hdrs.CONTENT_TYPE: 'text/html; charset=utf-8'})


@routes.get('/ws')
async def websocket_handler(request):
    app = request.app
    session = request['session']
    config = app['config']
    broadcaster = app['broadcaster']
    user_id = session['user'].user_id if session['user'] else 'anon'

    ws = web.WebSocketResponse()
    await ws.prepare(request)
    wsid = broadcaster.add_ws(ws)
    log.info('WebSocket connection %i (user %s) opened', wsid, user_id)

    await ws.send_json(get_auth_config_message(config.auth, session))
    await ws.send_json(await list_talks(app['talks']), dumps=json_dumps)
    if session['user']:
        await ws.send_json(
            await questions_voted_by_user(app['questions'], user_id))

    try:
        async for msg in ws:
            if msg.type == WSMsgType.TEXT:
                body = msg.json()
                log.debug('WS message from %i (user %s): %r',
                          wsid, user_id, body)

                body['app'] = app
                body['session'] = session
                msg_req = MsgRequest(**body)

                handler = msg_routes.resolve(msg_req)
                response = await handler(msg_req)
                if response:
                    msg_req.responses.append(response)

                for response in msg_req.responses:
                    await ws.send_json(response, dumps=json_dumps)

                if msg_req.type in ('password_login', 'logout'):
                    user_id = (
                        session['user'].user_id if session['user'] else 'anon')

            elif msg.type == WSMsgType.ERROR:
                log.error('WebSocket closed with %s', ws.exception())
            else:
                log.error('Unknown WS message %r from %i (user %s)',
                          body, wsid, user_id)
    except CancelledError:
        raise
    except Exception:
        log.exception('WS %i (user %s) failed', wsid, user_id)
    finally:
        log.info('WebSocket connection %i (user %s) closed', wsid, user_id)
        broadcaster.remove_ws(wsid)
        return ws
