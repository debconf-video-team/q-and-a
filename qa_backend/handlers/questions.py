import logging

from qa_backend.utils import frontend_error
from qa_backend.ws import msg_routes
from qa_backend.handlers.users import users_details, broadcast_user_update


log = logging.getLogger(__name__)


async def list_talk_questions(questions_mgr, talk_id):
    """List all questions for a given talk"""
    questions = await questions_mgr.get_talk_questions(talk_id)
    return {
        'type': 'questions_talk_list',
        'data': {question_id: question.as_ws_dict()
                 for question_id, question in questions.items()},
    }

def broadcast_question_update(broadcaster, question):
    broadcaster.broadcast({
        'type': 'question_update',
        'data': question.as_ws_dict(),
    })


@msg_routes.type('questions_list')
async def questions_list(request):
    talk_id = request.data['talk_id']
    questions = await list_talk_questions(request.app['questions'], talk_id)
    request.add_response(questions)
    user_ids = [question['author'] for question in questions['data'].values()]
    users = await users_details(request.app['users'], user_ids)
    request.add_response(users)


@msg_routes.type('question_post')
async def question_post(request):
    user = request.session['user']
    if not user:
        log.warn("Refusing to let an anonymous user ask a question")
        return frontend_error("Cannot ask questions anonymously")
    if not user.activated:
        log.warn("Refusing to let a non-activated user ask a question")
        return frontend_error("Cannot ask questions until activated")
    body = request.data['body']
    talk_id = request.data['talk_id']
    author = user.user_id
    question_mgr = request.app['questions']
    question = await question_mgr.ask_question(talk_id, body, author)
    broadcaster = request.app['broadcaster']
    broadcast_question_update(broadcaster, question)
    broadcast_user_update(broadcaster, user)


@msg_routes.type('question_vote')
async def question_vote(request):
    user = request.session['user']
    if not user:
        log.warn("Refusing to let an anonymous user vote")
        return frontend_error("Cannot vote anonymously")
    if not user.activated:
        log.warn("Refusing to let a non-activated user vote")
        return frontend_error("Cannot vote until activated")
    user_id = user.user_id
    question_id = request.data['question_id']
    question = await request.app['questions'].get_by_id(question_id)
    if question.author == user_id:
        log.warn("Refusing to let a user self-vote")
        return frontend_error("Cannot vote for your own questions")
    await request.app['questions'].vote(question_id, user_id)
    broadcast_question_update(request.app['broadcaster'], question)


@msg_routes.type('question_unvote')
async def question_unvote(request):
    user = request.session['user']
    if not user:
        log.warn("Refusing to let an anonymous user vote")
        return frontend_error("Cannot vote anonymously")
    if not user.activated:
        log.warn("Refusing to let a non-activated user vote")
        return frontend_error("Cannot vote until activated")
    question_id = request.data['question_id']
    user_id = user.user_id
    await request.app['questions'].unvote(question_id, user_id)
    question = await request.app['questions'].get_by_id(question_id)
    broadcast_question_update(request.app['broadcaster'], question)


async def questions_voted_by_user(questions, user_id):
    votes = await questions.voted_by_user(user_id)
    return {
        'type': 'questions_voted_complete_list',
        'data': list(votes),
    }


@msg_routes.type('questions_voted')
async def questions_voted(request):
    user = request.session['user']
    if not user:
        return
    return await questions_voted_by_user(request.app['questions'], user.user_id)
