import logging

log = logging.getLogger(__name__)


async def users_details(users_mgr, user_ids):
    """List details for the given set of User IDs"""
    users = await users_mgr.get_by_ids(user_ids)
    return {
        'type': 'users_list_partial',
        'data': {user_id: user.as_ws_dict()
                 for user_id, user in users.items()},
    }

def broadcast_user_update(broadcaster, user):
    broadcaster.broadcast({
        'type': 'user_update',
        'data': user.as_ws_dict(),
    })
