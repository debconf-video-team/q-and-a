from qa_backend.ws import msg_routes


async def list_talks(talks):
    """List all known talks"""
    talks = await talks.get_all()
    return {
        'type': 'talks_complete_list',
        'data': {talk_id: talk.as_ws_dict() for talk_id, talk in talks.items()},
    }


@msg_routes.type('talks_refresh')
async def talks_refresh(request):
    return await list_talks(request.app['talks'])
