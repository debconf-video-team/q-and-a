import logging

from aiohttp import web

from qa_backend.ws import msg_routes

log = logging.getLogger(__name__)


routes = web.RouteTableDef()


@routes.get('/login')
async def login_route(request):
    raise web.HTTPNotFound(reason='auth is done over the websocket')


@msg_routes.type('password_login')
async def login(request):
    email = request.data.get('email', '')
    password = request.data.get('password', '')

    user = await request.app['users'].password_auth(email, password)
    if not user:
        log.info('Authentication failure for %s', email)
        return {
            'type': 'auth_failure',
            'data': {
                'error': 'Authentication failed',
            }
        }

    log.info('Login: %s (%i)', user.fullname, user.user_id)
    request.session['user'] = user
    return {
        'type': 'auth_complete',
        'data': {
            'user': user.as_ws_dict(),
        }
    }


def get_auth_required_dict(auth_config):
    return {}
