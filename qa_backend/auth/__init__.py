from importlib import import_module


from qa_backend.auth.routes import auth_disabled_routes
from qa_backend.ws import msg_routes


def get_auth_module(auth_config):
    """Load the configured auth module"""
    if not auth_config:
        return None
    return import_module(f'qa_backend.auth.{auth_config.type}')


def get_auth_required_dict(auth_config):
    """Return a dict describing the configured auth module to the frontend"""
    mod = get_auth_module(auth_config)
    if not mod:
        return {'error': 'Auth is not configured'}
    auth_required_dict = mod.get_auth_required_dict(auth_config)
    auth_required_dict.setdefault('type', auth_config.type)
    return auth_required_dict


def get_auth_routes(auth_config):
    """Load the auth routes for the configured auth module"""
    mod = get_auth_module(auth_config)
    if not mod:
        return auth_disabled_routes
    return mod.routes


def get_auth_config_message(auth_config, session):
    authenticated = session['user'] is not None
    user_dict = session['user'].as_ws_dict() if authenticated else {}
    return {
        'type': 'auth_set_config',
        'data': {
            'authenticated': authenticated,
            'mechanism': get_auth_required_dict(auth_config),
            'user': user_dict,
        },
    }


@msg_routes.type('logout')
async def logout(request):
    request.session['user'] = None
    return get_auth_config_message(request.app['config'].auth, request.session)
