import logging

from aiohttp import ClientSession, hdrs, web

from oauthlib.oauth2 import WebApplicationClient
from oauthlib.common import generate_token

log = logging.getLogger(__name__)

routes = web.RouteTableDef()


@routes.get('/login')
async def login(request):
    config = request.app['config']
    redirect_uri = f'{config.server_url}/login/complete'
    client = WebApplicationClient(config.auth.client_id)
    state = generate_token()
    dest = client.prepare_request_uri(
        f'{config.auth.url}/oauth/authorize', state=state,
        scope='openid', redirect_uri=redirect_uri)
    if 'next' in request.query:
        request['session']['next'] = request.query['next']
    response = web.Response(status=302, headers={hdrs.LOCATION: dest})
    response.set_cookie('oauth2-state', state, httponly=True)
    return response


@routes.get('/login/complete')
async def login_complete(request):
    config = request.app['config']
    redirect_uri = f'{config.server_url}/login/complete'
    state = request.cookies['oauth2-state']
    client = WebApplicationClient(config.auth.client_id)
    result = client.parse_request_uri_response(
        f'{config.server_url}{request.path_qs}', state)
    code = result['code']
    async with ClientSession() as session:
        r = await session.post(f'{config.auth.url}/oauth/token', data={
            'client_id': config.auth.client_id,
            'client_secret': config.auth.client_secret,
            'code': code,
            'redirect_uri': redirect_uri,
            'grant_type': 'authorization_code',
        })
        if not r.status == 200:
            raise Exception('Failed to retrieve OAuth2 token')
        token = await r.json()

        auth_headers = {hdrs.AUTHORIZATION: f'Bearer {token["access_token"]}'}
        r = await session.get(f'{config.auth.url}/oauth/userinfo',
                              headers=auth_headers)
        if not r.status == 200:
            raise Exception('Failed to retrieve UserInfo')
        userinfo = await r.json()

    if config.auth.group and config.auth.group not in userinfo['groups']:
        raise web.HTTPForbidden(
            reason=f'Access Denied. Not a member of {config.auth.group}.')

    user_data = {
        'avatar_url': userinfo['picture'],
        'email': userinfo.get('email'),
        'fullname': userinfo['name'],
        'sso_id': int(userinfo['sub']),
    }
    user = await request.app['users'].sso_auth(user_data)
    request['session']['user'] = user

    next_url = '/'
    if 'next' in request['session']:
        next_url = request['session']['next']
    response = web.Response(status=302, headers={hdrs.LOCATION: next_url})
    response.del_cookie('oauth2-state')
    return response


def get_auth_required_dict(auth_config):
    return {
        'name': auth_config.name,
        'url': auth_config.url,
    }
