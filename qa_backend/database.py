from dataclasses import asdict

from aiopg.sa import create_engine
import sqlalchemy as sa


metadata = sa.MetaData()

talks = sa.Table('talks', metadata,
    sa.Column('talk_id', sa.SmallInteger, primary_key=True),
    sa.Column('title', sa.UnicodeText, nullable=False),
    sa.Column('speakers', sa.UnicodeText, nullable=False),
    sa.Column('status',
              sa.Enum('active', 'closed', name='status'),
              nullable=False),
    sa.Column('start_at', sa.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('end_at', sa.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('created_at', sa.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('updated_at', sa.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('deleted_at', sa.TIMESTAMP(timezone=True), nullable=True),
)

questions = sa.Table('questions', metadata,
    sa.Column('question_id', sa.Integer, primary_key=True),
    sa.Column('talk_id',
              sa.SmallInteger,
              sa.ForeignKey('talks.talk_id'),
              nullable=False),
    sa.Column('body', sa.UnicodeText, nullable=False),
    sa.Column('author', sa.Integer, nullable=False),
    sa.Column('created_at', sa.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('updated_at', sa.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('deleted_at', sa.TIMESTAMP(timezone=True), nullable=True),
    sa.Column('answered_at', sa.TIMESTAMP(timezone=True), nullable=True),
)

users = sa.Table('users', metadata,
    sa.Column('user_id', sa.Integer, primary_key=True),
    sa.Column('fullname', sa.String, nullable=False),
    sa.Column('password_hash', sa.String, nullable=True),
    sa.Column('avatar_url', sa.String, nullable=False),
    sa.Column('sso_id', sa.Integer, nullable=True),
    sa.Column('email', sa.Unicode, nullable=True, unique=True),
    sa.Column('permissions',
              sa.Enum('admin', 'moderator', 'user', name='permissions'),
              nullable=False),
    sa.Column('activated', sa.Boolean, nullable=False),
    sa.Column('created_at', sa.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('updated_at', sa.TIMESTAMP(timezone=True), nullable=False),
    sa.Column('deleted_at', sa.TIMESTAMP(timezone=True), nullable=True),
)

votes = sa.Table('votes', metadata,
    sa.Column('vote_id', sa.Integer, primary_key=True),
    sa.Column('question_id',
              sa.Integer,
              sa.ForeignKey('questions.question_id'),
              nullable=False),
    sa.Column('user_id',
              sa.Integer,
              sa.ForeignKey('users.user_id'),
              nullable=False),
    sa.UniqueConstraint('question_id', 'user_id'),
)


def connect_sync(config):
    """Return a synchronous sqlalchemy engine"""
    sa_url = sa.engine.url.URL(
        drivername='postgresql',
        database=config.database,
        username=config.user,
        password=config.password,
        host=config.host,
    )
    return sa.create_engine(sa_url)


def create_schema(config):
    """Synchronously create the schema"""
    sync_engine = connect_sync(config)
    metadata.create_all(sync_engine)



async def connect_database(app):
    config = app['config']
    create_schema(config.database)
    engine = await create_engine(**asdict(config.database))
    app['database'] = engine


async def disconnect_database(app):
    app['database'].close()
    await app['database'].wait_closed()
