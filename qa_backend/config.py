import configparser
from dataclasses import dataclass
from typing import List, Optional


@dataclass
class Database:
    host: Optional[str]
    database: str
    user: Optional[str]
    password: Optional[str]


class AuthConfig:
    pass


@dataclass
class Config:
    database: Database
    auth: AuthConfig
    server_bind: str
    server_port: int
    server_url: Optional[str]


@dataclass
class AuthGitLab(AuthConfig):
    type = 'gitlab'
    client_id: str
    client_secret: str
    group: Optional[List[str]]
    name: str
    url: str


@dataclass
class AuthPassword(AuthConfig):
    type = 'password'


def parse_config_list(config, key):
    text = config.get(key)
    return [item.strip() for item in text.split(',') if item.strip()]


def parse_config(config_file):
    cfgp = configparser.ConfigParser()
    cfgp.read(config_file)
    auth = None
    database = None
    for section_name in cfgp.sections():
        section = cfgp[section_name]
        if section_name.startswith('auth:') and auth is not None:
            raise Exception(
                'Only a single auth backend can be enabled in config.')
        if section_name == 'auth:gitlab':
            auth = AuthGitLab(
                client_id=section.get('client_id'),
                client_secret=section.get('client_secret'),
                group=section.get('group'),
                name=section.get('name'),
                url=section.get('url'),
            )
        elif section_name == 'auth:password':
            auth = AuthPassword()
        if section_name == 'database':
            database = Database(
                host=section.get('host'),
                database=section['database'],
                user=section.get('user'),
                password=section.get('password'),
            )
    q_and_a = cfgp['q_and_a']
    config = Config(
        auth=auth,
        database=database,
        server_bind=q_and_a['server_bind'],
        server_port=q_and_a['server_port'],
        server_url=q_and_a.get('server_url'),
    )
    return config
