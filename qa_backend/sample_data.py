import logging
from datetime import datetime

from passlib.hash import bcrypt
from sqlalchemy.sql import func

from qa_backend.database import connect_sync, talks, questions, users, votes


log = logging.getLogger(__name__)


def load_sample_data(config):
    try:
        from zoneinfo import ZoneInfo
    except ImportError:
        from backports.zoneinfo import ZoneInfo

    engine = connect_sync(config.database)

    USERS = (
        {'fullname': 'Test 1', 'email': 'test1@example.com',
         'password_hash': bcrypt.hash('test')},
        {'fullname': 'Test 2', 'email': 'test2@example.com'},
        {'fullname': 'Test 3', 'email': 'test3@example.com'},
    )

    user_ids = []
    for user in USERS:
        user.setdefault('avatar_url', 'https://example.com/')
        user.setdefault('permissions', 'user')
        user.setdefault('activated', True)
        r = engine.execute(users.insert().values(
            created_at=func.current_timestamp(),
            updated_at=func.current_timestamp(),
            **user))
        user_ids.append(r.inserted_primary_key[0])

    UTC = ZoneInfo('UTC')
    TALKS = (
        {'title': 'Talk 1', 'speakers': 'Talk 1 Speakers',
         'start_at': datetime(2021, 10, 1, 10, 00, tzinfo=UTC),
         'end_at': datetime(2021, 10, 1, 10, 30, tzinfo=UTC)},
        {'title': 'Talk 2', 'speakers': 'Talk 2 Speakers',
         'start_at': datetime(2021, 10, 1, 10, 30, tzinfo=UTC),
         'end_at': datetime(2021, 10, 1, 11, 00, tzinfo=UTC)},
        {'title': 'Talk 3', 'speakers': 'Talk 3 Speakers',
         'start_at': datetime(2021, 10, 1, 11, 00, tzinfo=UTC),
         'end_at': datetime(2021, 10, 1, 11, 30, tzinfo=UTC)},
    )
    talk_ids = []
    for talk in TALKS:
        talk.setdefault('status', 'active')
        r = engine.execute(talks.insert().values(
            created_at=func.current_timestamp(),
            updated_at=func.current_timestamp(),
            **talk))
        talk_ids.append(r.inserted_primary_key[0])

    QUESTIONS = (
        {'body': 'What is this about?', 'talk_id': 0, 'author': 0},
        {'body': 'Did anyone understand anything?', 'talk_id': 0, 'author': 1},
        {'body': 'I see what you did there.', 'talk_id': 1, 'author': 0},
    )

    question_ids = []
    for question in QUESTIONS:
        question['author'] = user_ids[question['author']]
        question['talk_id'] = talk_ids[question['talk_id']]
        r = engine.execute(questions.insert().values(
            created_at=func.current_timestamp(),
            updated_at=func.current_timestamp(),
            **question))
        question_ids.append(r.inserted_primary_key[0])

    VOTES = (
        {'question_id': 0, 'user_id': 1},
        {'question_id': 0, 'user_id': 2},
    )
    for vote in VOTES:
        vote['question_id'] = question_ids[vote['question_id']]
        vote['user_id'] = user_ids[vote['user_id']]
        r = engine.execute(votes.insert().values(**vote))
    log.warning("Sample data appended")
