from aiohttp import web

from qa_backend.auth import get_auth_routes
from qa_backend.auth.middleware import session_middleware
from qa_backend.broadcaster import WSBroadcaster
from qa_backend.database import connect_database, disconnect_database
from qa_backend.models import initialize_managers
from qa_backend.routes import routes


async def app_factory(config):
    middlewares = [session_middleware]
    app = web.Application(middlewares=middlewares)
    app.add_routes(routes)
    app.add_routes(get_auth_routes(config.auth))
    app['sessions'] = {}
    app['config'] = config
    app['broadcaster'] = WSBroadcaster()
    app.on_startup.append(connect_database)
    app.on_startup.append(initialize_managers)
    app.on_cleanup.append(disconnect_database)
    return app
