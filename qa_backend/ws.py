from dataclasses import dataclass, field
import logging

from aiohttp import web


log = logging.getLogger(__name__)


@dataclass
class MsgRequest:
    """Websocket incoming message (similar to aiohttp.web.Request)"""
    type: str
    data: dict
    app: web.Application
    session: dict
    responses: list = field(default_factory=list)

    def add_response(self, response):
        self.responses.append(response)


class MsgRouter:
    """Websocket message router (similar to aiohttp.web.Router)"""

    def __init__(self):
        self._handlers = {}

    def resolve(self, request: MsgRequest):
        """Locate the handler for request"""
        if request.type in self._handlers:
            return self._handlers[request.type]
        else:
            log.error("Unhandled request type %s", request.type)

    def type(self, type_):
        """Add a handler for type_"""
        def decorator(func):
            self._handlers[type_] = func
            return func
        return decorator


msg_routes = MsgRouter()
