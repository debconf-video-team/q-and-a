from asyncio import create_task
from itertools import count
from weakref import WeakValueDictionary

from qa_backend.utils import json_dumps


class WSBroadcaster:
    def __init__(self):
        self.wsid_iter = count()
        self.websockets = WeakValueDictionary()
        self.tasks = set()

    def add_ws(self, ws):
        wsid = next(self.wsid_iter)
        self.websockets[wsid] = ws
        self.broadcast_connected_users()
        return wsid

    def remove_ws(self, wsid):
        self.websockets.pop(wsid)
        self.broadcast_connected_users()

    def broadcast_connected_users(self):
        self.broadcast({
            'type': 'connected_users',
            'user_count': len(self.websockets),
        })
        # TODO: Send moderators actual user session lists

    def broadcast(self, message):
        data = json_dumps(message)
        for ws in self.websockets.values():
            task = create_task(ws.send_str(data))
            self.tasks.add(task)
            task.add_done_callback(self.tasks.discard)
