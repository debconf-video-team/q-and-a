import argparse
import logging

from aiohttp import web

from qa_backend.app import app_factory
from qa_backend.config import parse_config
from qa_backend.database import create_schema
from qa_backend.sample_data import load_sample_data


def main():
    p = argparse.ArgumentParser('Q&A Backend')
    p.add_argument('-v', '--verbose', action='store_true',
                   help='Increase verbosity')
    p.add_argument('-b', '--bind', metavar='ADDRESS',
                   help='Listen on ADDRESS:PORT')
    p.add_argument('-p', '--port', metavar='PORT', type=int, default=None,
                   help='Listen on ADDRESS:PORT')
    p.add_argument('--sample-data', action='store_true',
                   help='Load sample data')
    p.add_argument('-c', '--config', help='Configuration file')
    args = p.parse_args()

    config = parse_config(args.config)

    if args.sample_data:
        create_schema(config.database)
        load_sample_data(config)

    logging.basicConfig(level=logging.DEBUG if args.verbose else logging.INFO)
    app = app_factory(config)

    web.run_app(
        app,
        host=args.bind or config.server_bind,
        port=args.port or config.server_port,
    )


if __name__ == '__main__':
    main()
