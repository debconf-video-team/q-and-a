# Q & A

A Q&A web app for collecting conference talk questions.

## Development

There are two parts to `q_and_a`, a Python asyncio backend, and a Vue.js
frontend. They communicate via a websocket.

### Frontend

There is a **README** for frontend in its directory.
It can be developed stand-alone against a remote backend or locally.

### Backend

1. Install the python requirements (Debian packages or virtualenv).
1. Create a postgres database, or use `pg_virtualenv`
1. Copy `example.ini` and edit it.
1. Run `q_and_a`: `python -m q_and_a -c config.ini`
1. Browse to http://127.0.0.1:8080/
