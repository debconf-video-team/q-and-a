from aiopg.sa.url import URL
from aiopg.sa.engine import Engine

def create_engine(url: URL) -> Engine: ...
