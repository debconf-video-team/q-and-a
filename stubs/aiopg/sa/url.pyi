from typing import Optional


class URL:
    def __init__(self, drivername: Optional[str], database: Optional[str],
                 username: Optional[str], password: Optional[str],
                 host: Optional[str]): ...
