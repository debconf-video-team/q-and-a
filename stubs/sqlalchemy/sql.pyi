from typing import List

from sqlalchemy import Column


class _FunctionGenerator:
    def count(self, Column): ...
    def current_timestamp(self): ...

func: _FunctionGenerator

def select(columns: List[Column]): ...
