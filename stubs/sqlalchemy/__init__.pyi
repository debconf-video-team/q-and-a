from typing import Any, Type, Union


class Constraint: ...

class MetaData: ...

class TypeEngine: ...

class ForeignKey:
    def __init__(self, target: str): ...

class _WhereClause:
    def __and__(self, other: '_WhereClause') -> '_WhereClause': ...

class Column:
    def __init__(
            self,
            name: str,
            type_: Union[Type[TypeEngine], TypeEngine],
            foreign_key: ForeignKey = None,
            primary_key: bool = False,
            nullable: bool = True,
            unique: bool = False,
        ): ...
    def __eq__(self, other: Any) -> _WhereClause: ...  # type: ignore[override]

class _Columns:
    def __getattr__(self, name: str) -> Column: ...

class Table:
    c: _Columns
    def __init__(
            self,
            name: str,
            metadata: MetaData,
            *definition: Union[Column, Constraint],
        ): ...
    def delete(self, *where_clause: _WhereClause): ...
    def insert(self, *where_clause: _WhereClause): ...
    def join(self, Table): ...
    def select(self, *where_clause: _WhereClause): ...
    def update(self, *where_clause: _WhereClause): ...

class Boolean(TypeEngine): ...

class Enum(TypeEngine):
    def __init__(self, *values: str, name: str): ...

class Integer(TypeEngine): ...

class SmallInteger(TypeEngine): ...

class String(TypeEngine): ...

class TIMESTAMP(TypeEngine):
    def __init__(self, timezone: bool = False): ...

class Unicode(TypeEngine): ...

class UnicodeText(TypeEngine): ...

class UniqueConstraint(Constraint):
    def __init__(self, *columns: str): ...
