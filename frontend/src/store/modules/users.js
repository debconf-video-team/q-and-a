import Vue from 'vue';

const state = () => ({
  users_by_id: {},
});

const mutations = {
  users_list_partial(state, users_by_id) {
    for (const [user_id, user] of Object.entries(users_by_id)) {
      Vue.set(state.users_by_id, user_id, user);
    }
  },
  user_update(state, user) {
    Vue.set(state.users_by_id, user.user_id, user);
  },
};

const actions = {
};

export default {
  actions,
  mutations,
  state,
};
