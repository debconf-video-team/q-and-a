const state = () => ({
  authenticated: false,
  authenticating: false,
  error_message: null,
  sso_name: null,
  sso_url: null,
  type: null,
  user: {},
});

const mutations = {
  auth_set_config(state, config) {
    state.authenticating = false;
    state.error_message = null;
    state.authenticated = config.authenticated;
    state.user = config.user;
    state.type = config.mechanism.type;
    if (config.mechanism.type == 'gitlab') {
      state.sso_name = config.mechanism.name;
      state.sso_url = config.mechanism.url;
    }
  },
  auth_failure(state, data) {
    state.authenticated = false;
    state.authenticating = false;
    state.error_message = data.error;
  },
  auth_submit(state) {
    state.authenticating = true;
  },
  auth_complete(state, data) {
    state.authenticating = false;
    state.authenticated = true;
    state.error_message = null;
    state.user = data.user;
  },
};

const actions = {
  auth_logout({dispatch}) {
    dispatch('ws_post', {
      type: 'logout',
      data: {},
    });
  },
  auth_password_login({commit, dispatch}, {email, password}) {
    commit('auth_submit');
    dispatch('ws_post', {
      type: 'password_login',
      data: {
        email,
        password,
      },
    });
  },
};

export default {
  actions,
  mutations,
  state,
};
