import Vue from 'vue';

const state = () => ({
  questions_by_id: {},
  questions_voted: {},
});

const mutations = {
  questions_talk_list(state, questions_by_id) {
    state.questions_by_id = questions_by_id;
  },
  questions_voted_complete_list(state, questions_voted) {
    let obj = {};
    for (const question_id of questions_voted) {
      obj[question_id] = true;
    }
    state.questions_voted = obj;
  },
  question_vote(state, question_id) {
    Vue.set(state.questions_voted, question_id, true);
  },
  question_unvote(state, question_id) {
    Vue.set(state.questions_voted, question_id, false);
  },
  question_update(state, question) {
    Vue.set(state.questions_by_id, question.question_id, question);
  },
};

const actions = {
  questions_list({dispatch}, talk_id) {
    dispatch('ws_post', {
      type: 'questions_list',
      data: {talk_id},
    });
  },
  question_post({dispatch}, {talk_id, body}) {
    dispatch('ws_post', {
      type: 'question_post',
      data: {talk_id, body},
    });
  },
  questions_voted({dispatch}) {
    dispatch('ws_post', {
      type: 'questions_voted',
      data: {},
    });
  },
  question_vote({commit, dispatch}, question_id) {
    dispatch('ws_post', {
      type: 'question_vote',
      data: {question_id},
    });
    commit('question_vote', question_id);
  },
  question_unvote({commit, dispatch}, question_id) {
    dispatch('ws_post', {
      type: 'question_unvote',
      data: {question_id},
    });
    commit('question_unvote', question_id);
  },
};

export default {
  actions,
  mutations,
  state,
};
