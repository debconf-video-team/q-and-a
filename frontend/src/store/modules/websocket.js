const state = () => ({
  connection: 'disconnected',
  state_last_updated: null,
  ws: null,
  queue: [],
});

const mutations = {
  connecting(state, ws) {
    state.connection = 'connecting';
    state.ws = ws;
  },

  connected(state) {
    state.connection = 'connected';
  },

  disconnected(state) {
    state.connection = 'disconnected';
    state.ws = null;
  },

  state_updated(state) {
    state.state_last_updated = new Date();
  },

  enqueue(state, msg) {
    // Try to avoid the queue growing infinitely, we probably only need the
    // last of each type of message
    state.queue.filter(m => m.type != msg.type);
    state.queue.push(msg);
  },

  flush_queue(state) {
    state.queue = [];
  },
};

const actions = {
  connect({commit, dispatch, state}) {
    if (state.connection != 'disconnected') {
      return;
    }
    const url = location.href.replace(/^http/, 'ws').replace(/#.*/, '') + 'ws';
    const ws = new WebSocket(url);
    ws.binaryType = 'arraybuffer';
    ws.addEventListener('message', ev => dispatch('ws_message_raw', ev));
    ws.addEventListener('close', ev => dispatch('ws_close', ev));
    ws.addEventListener('error', ev => dispatch('ws_error', ev));
    ws.addEventListener('open', ev => dispatch('connected', ev));
    commit('connecting', ws);
  },

  connected({commit, dispatch, state}) {
    commit('connected');
    commit('error_remove', 'ws');
    for (const msg of state.queue) {
      dispatch('ws_post', msg);
    }
    commit('flush_queue');
  },

  ws_message_raw({dispatch}, ev) {
    if (typeof ev.data == 'string') {
      const body = JSON.parse(ev.data);
      dispatch('ws_message_deserialized', body);
    } else {
      console.log('Unexpected message', ev);
    }
  },

  ws_message_deserialized({commit}, body) {
    switch (body.type) {
      case 'auth_complete':
      case 'auth_failure':
      case 'auth_set_config':
      case 'error_add':
      case 'question_update':
      case 'questions_talk_list':
      case 'questions_voted_complete_list':
      case 'talks_complete_list':
      case 'users_list_partial':
      case 'user_update':
        commit(body.type, body.data);
        break;
      default:
        console.log('Unhandled message', body);
        break;
    }
    commit('state_updated');
  },

  ws_error({commit}) {
    commit('error_add', {
      key: 'ws',
      message: 'WebSocket Error',
      priority: 100,
    });
    commit('disconnected');
  },

  ws_close({commit, dispatch}) {
    commit('error_add', {
      key: 'ws',
      message: 'WebSocket Closed',
      priority: 100,
    });
    commit('disconnected');
    setTimeout(() => dispatch('connect'), 1000);
  },

  ws_post({commit, state}, message) {
    if (state.connection == 'connected') {
      state.ws.send(JSON.stringify(message));
    } else {
      commit('enqueue', message);
    }
  },
};

export default {
  actions,
  mutations,
  state,
};
