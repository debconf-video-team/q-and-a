const state = () => ({
  talks_by_id: {},
});

const mutations = {
  talks_complete_list(state, talks_by_id) {
    for (let talk of Object.values(talks_by_id)) {
      talk.start_at = new Date(talk.start_at);
      talk.end_at = new Date(talk.end_at);
    }
    state.talks_by_id = talks_by_id;
  },
};

const actions = {
  talks_refresh({dispatch}) {
    dispatch('ws_post', {
      type: 'talks_refresh',
      data: {},
    });
  },
};

export default {
  actions,
  mutations,
  state,
};
