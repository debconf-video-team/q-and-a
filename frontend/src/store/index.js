import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import errors from './modules/errors';
import talks from './modules/talks';
import questions from './modules/questions';
import users from './modules/users';
import websocket from './modules/websocket';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    errors,
    talks,
    questions,
    users,
    websocket,
  },
  strict: process.env.NODE_ENV !== 'production',
});
