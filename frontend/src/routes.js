import VueRouter from 'vue-router';

import LoginModal from './components/LoginModal.vue';
import Talk from './components/Talk.vue';
import TalkList from './components/TalkList.vue';

const routes = [
  {path: '/', component: TalkList},
  {path: '/login', component: LoginModal},
  {path: '/talks/:id', component: Talk, props: true},
];

const router = new VueRouter({
  routes,
});

export {routes, router};
