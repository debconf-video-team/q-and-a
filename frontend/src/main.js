import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import Vue from 'vue';

import QA from './QA.vue';
import store from './store';
import {router} from './routes';

Vue.config.productionTip = false;

new Vue({
  store,
  render: h => h(QA),
  router,
}).$mount('#app');
